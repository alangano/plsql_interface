#!/usr/bin/env python3

import os, sys
t_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
os.environ['APP_ROOT'] = t_dir
sys.path.insert(0,f"{os.environ['APP_ROOT']}/lib")

import oracledb

import ora_pool
ora_pool.start()



with ora_pool.acquire() as conn:
   conn.pi.test1.proc2(v1=1,v2=2)
   conn.pi.test1.proc2(1,v2=2)
   t_out = conn.pi.test1.proc3(1,[int])
   print(t_out)
   t_out = conn.pi.test1.proc3(5,v2=[int])
   print(t_out)
   t_out = conn.pi.test1.proc4(5,[int,3])
   print(t_out)
   t_out = conn.pi.test1.proc4(5,o1=[int,3])
   print(t_out)
   t_out = conn.pi.test1.proc5([int],[int])
   print(t_out)
   t_id = conn.pi.r(int).test1.func1()
   print(t_id)
   _out = conn.pi.r(int).test1.func2(o1=[str,'wat'],i1=100,i2=50)
   rint(t_out)
   t_cur = conn.pi.r(oracledb.CURSOR).test1.func3()
   print(type(t_cur))
   print(t_cur)

"""
create or replace package test1 is
   PROCEDURE proc1;
   PROCEDURE proc2(v1 IN integer, v2 IN integer);
   PROCEDURE proc3(i1 IN integer, o2 OUT integer);
   PROCEDURE proc4(i1 IN integer, io1 IN OUT integer);
   PROCEDURE proc5(o1 OUT integer, o2 OUT integer);
   FUNCTION func1 return integer;
   FUNCTION func2(
      o1 IN OUT varchar2,
      i1 IN integer,
      i2 IN integer
   ) return integer;
   FUNCTION func3 return sys_refcursor;
end;
/

create or replace package body test1 is
   PROCEDURE proc1 is begin null; end;
   PROCEDURE proc2(v1 IN integer, v2 IN integer) is begin null; end;
   PROCEDURE proc3(i1 IN integer, o2 OUT integer) is begin o2 := i1 + 10; end;
   PROCEDURE proc4(i1 IN integer, io1 IN OUT integer) is
   begin
      io1 := i1 + io1;
   end;
   PROCEDURE proc5(o1 OUT integer, o2 OUT integer) is
   begin
      o1 := 10;
      o2 := 100;
   end;
   FUNCTION func1 return integer is
   begin 
      return 69;
   end;
   FUNCTION func2(
      o1 IN OUT varchar2,
      i1 IN integer,
      i2 IN integer
   ) return integer is
   begin
      o1 := upper(o1);
      return i1 + i2;
   end;
   FUNCTION func3 return sys_refcursor is
      t_cur sys_refcursor;
   begin
      open t_cur for select object_name, object_type from user_objects;
      return t_cur;
   end;
end;
/

begin
   test1(v1 = 1,v2 = 2);
end;
/

"""


