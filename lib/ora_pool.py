
import oracledb

from  plsql_interface import PLSQLInterface

pool = None

#------------------------------------------------------------------------------
def init_session(i_connection, i_requestedTag_ignored):
   i_connection.module = 'pi test'
   i_connection.cursor().execute(
      "alter session set nls_date_format = 'YYYY-MM-DD HH24:MI:SS'"
   )

#------------------------------------------------------------------------------
class EPHDBConnection(oracledb.Connection):
   def __init__(self,*i_args,**i_nargs):
      super().__init__(*i_args,**i_nargs)

   def __getattribute__(self,i_attr):
      if i_attr == 'pi':
         return PLSQLInterface(self)
      else:
         return super().__getattribute__(i_attr)

#------------------------------------------------------------------------------
def start():
   global pool

   pool = oracledb.SessionPool(
      user              = 'user',
      password          = 'password',
      dsn               = 'testdb',
      min               = 1,
      max               = 5,
      increment         = 1,
      threaded          = True,
      getmode           = oracledb.SPOOL_ATTRVAL_WAIT,
      session_callback  = init_session,
      events            = True,
      connectiontype    = EPHDBConnection
   )

#------------------------------------------------------------------------------
def acquire():
   return pool.acquire()

#------------------------------------------------------------------------------

