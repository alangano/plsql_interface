
class ObjectDict(dict):
   def __getattr__(self,n):
      return self[n]

   def __setattr__(self,n,v):
      self[n] = v

   def _make(self,i_dict,i_follow_lists=True):
      for k,v in i_dict.items():
         if type(v) == dict:
            self[k] = ObjectDict()
            self[k]._make(v,i_follow_lists)
         elif type(v) == list:
            if i_follow_lists:
               t_list = []
               for e in v:
                  if type(e) == dict:
                     qd = ObjectDict()
                     qd._make(e,i_follow_lists)
                     t_list.append(qd)
                  else:
                     t_list.append(e)
               self[k] = t_list
            else:
               self[k] = v
         else:
            self[k] = v

def make(i_dict,i_follow_lists=True):
   qd = ObjectDict()
   qd._make(i_dict,i_follow_lists)
   return qd


