
import oracledb

import object_dict

class PLSQLInterface():
   def __init__(self,i_conn):
      self.conn = i_conn
      self.path = []
      self.return_class = None

   def __getattr__(self,i_name):
      self.path.append(i_name)
      return self

   def r(self,i_class):
      self.return_class = i_class
      return self

   def __call__(self,*i_args,**i_nargs):
      t_call = '.'.join(self.path)
      t_return_class = self.return_class
      self.path.clear()
      self.return_class = None

      t_cursor = self.conn.cursor()
      t_binds = {}

      t_sql = []
      t_sql.append('begin')
      if t_return_class == None:
         t_sql.append(f"   {t_call}")
      else:
         t_sql.append(f"   :fresult := {t_call}")
         t_binds['fresult'] = t_cursor.var(t_return_class)

      if len(i_args) > 0 or len(i_nargs) > 0:
         t_sql[-1] += '('

      t_tmp = []
      for i,v in enumerate(i_args):
         i += 1
         if type(v) == list:
            t_tmp.append(f"\n      :p{i}")
            t_binds[f"p{i}"] = t_cursor.var(v[0])
            if len(v) == 2:
               t_binds[f"p{i}"].setvalue(0,v[1])
         else:
            t_tmp.append(f"\n      :p{i}")
            t_binds[f"p{i}"] = v

      t_sql[-1] += ','.join(t_tmp)

      if len(i_args) > 0 and len(i_nargs) > 0:
         t_sql[-1] += ','

      t_tmp = []
      for n,v in i_nargs.items():
         if type(v) == list:
            t_tmp.append(f"\n      {n} => :{n}")
            t_binds[n] = t_cursor.var(v[0])
            if len(v) == 2:
               t_binds[n].setvalue(0,v[1])
         else:
            t_tmp.append(f"\n      {n} => :{n}")
            t_binds[n] = v

      t_sql[-1] += ','.join(t_tmp)

      if len(i_args) > 0 or len(i_nargs) > 0:
         t_sql[-1] += '\n   )'

      t_sql[-1] += ';'
      t_sql.append('end;')

      #print('-'*79)
      #print('\n'.join(t_sql))
      #print(t_binds)

      t_cursor.execute(
         ('\n'.join(t_sql)),
         t_binds
      )
      t_out = {}
      for n,b in t_binds.items():
         if type(b) == oracledb.Var:
            t_out[n] = b.getvalue()

      for n,cursor in t_out.items():
         # convert cursors to the cursor's result
         if type(cursor) == oracledb.Cursor:
            t_result = []
            t_column_names = [c[0].lower() for c in cursor.description]
            for r in cursor:
               t_result.append(object_dict.make(dict(zip(t_column_names,r))))

            t_out[n] = t_result

      if 'fresult' in t_out and len(t_out) == 1:
         return t_out['fresult']
      else:
         return t_out


